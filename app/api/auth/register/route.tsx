// @ts-nocheck

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const prisma = new PrismaClient();

export const POST = async (request: Request) => {
  try {
    const { username, password } = await request.json();

    const existingUser = await prisma.user.findFirst({
      where: { username: { equals: username } },
    });

    if (existingUser) {
      return NextResponse.error('User already exists', { status: 400 });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = await prisma.user.create({
      data: {
        username,
        password: hashedPassword,
      },
    });

    const token = jwt.sign({ userId: newUser.id }, 'your-secret-key', { expiresIn: '1h' });

    return NextResponse.json({ token, user: newUser }, { status: 201 });
  } catch (error) {
    console.error('Error registering user:', error);
    return NextResponse.error('Internal Server Error', { status: 500 });
  } finally {
    await prisma.$disconnect();
  }
};
