// @ts-nocheck

import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const prisma = new PrismaClient();

export const POST = async (request) => {
  try {
    const { username, password } = await request.json();

    const user = await prisma.user.findUnique({
      where: { username },
    });

    if (!user) {
      return NextResponse.error('Invalid credentials', { status: 401 });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return NextResponse.error('Invalid credentials', { status: 401 });
    }

    const token = jwt.sign({ userId: user.id }, 'your-secret-key', { expiresIn: '1h' });

    return NextResponse.json({ token, user }, { status: 200 });
  } catch (error) {
    console.error('Error during login:', error);
    return NextResponse.error('Internal Server Error', { status: 500 });
  } finally {
    await prisma.$disconnect();
  }
};
