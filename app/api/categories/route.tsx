// @ts-nocheck
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export const POST = async (request: Request) => {
    try {
        const body = await request.json();
        const category = await prisma.category.create({
            data: {
                name: body.name
            }
        });

        return NextResponse.json(category, { status: 200 });
    } catch (error) {
        console.error('Error adding category:', error);
        return NextResponse.error('Failed to add category', { status: 500 });
    }
};

export const GET = async () => {
    try {
        const categories = await prisma.category.findMany();
        return NextResponse.json(categories, { status: 200 });
    } catch (error) {
        console.error('Error fetching categories:', error);
        return NextResponse.error('Failed to fetch categories', { status: 500 });
    }
};
