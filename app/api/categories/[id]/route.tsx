// @ts-nocheck
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const PATCH = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const body = await request.json();
        const category = await prisma.category.update({
            where: {
                id: Number(params.id),
            },
            data: {
                name: body.name,
            },
        });

        return NextResponse.json(category, { status: 200 });
    } catch (error) {
        console.error("Error in PATCH:", error);
        return NextResponse.error("Internal Server Error", { status: 500 });
    } finally {
        await prisma.$disconnect();
    }
};

export const DELETE = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const category = await prisma.category.delete({
            where: {
                id: Number(params.id),
            },
        });

        return NextResponse.json(category, { status: 200 });
    } catch (error) {
        console.error("Error in DELETE:", error);
        return NextResponse.error("Internal Server Error", { status: 500 });
    } finally {
        await prisma.$disconnect();
    }
};

export const GET = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const category = await prisma.category.findUnique({
            where: {
                id: Number(params.id),
            },
            include: {
                books: true,
            },
        });

        if (!category) {
            return NextResponse.error("Category not found", { status: 404 });
        }

        return NextResponse.json(category, { status: 200 });
    } catch (error) {
        console.error("Error in GET:", error);
        return NextResponse.error("Internal Server Error", { status: 500 });
    } finally {
        await prisma.$disconnect();
    }
};
