// @ts-nocheck
import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const GET = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const book = await prisma.book.findUnique({
            where: {
                id: Number(params.id)
            },
        });

        if (book) {
            return NextResponse.json(book, { status: 200 });
        } else {
            return NextResponse.error('Book not found', { status: 404 });
        }
    } catch (error) {
        console.error('Error fetching book:', error);
        return NextResponse.error('Failed to fetch book', { status: 500 });
    }
};

export const PATCH = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const body = await request.json();

        let calculatedThickness = "";
        const totalPageInt = parseInt(body.total_page);

        if (totalPageInt <= 100) {
            calculatedThickness = "tipis";
        } else if (totalPageInt <= 200) {
            calculatedThickness = "sedang";
        } else {
            calculatedThickness = "tebal";
        }

        const book = await prisma.book.update({
            where: {
                id: Number(params.id),
            },
            data: {
                title: body.title,
                description: body.description,
                image_url: body.image_url,
                release_year: body.release_year,
                price: body.price,
                total_page: body.total_page,
                thickness: calculatedThickness,
                category_id: body.category_id,
            }
        });

        return NextResponse.json(book, { status: 200 });
    } catch (error) {
        console.error('Error updating book:', error);
        return NextResponse.error('Failed to update book', { status: 500 });
    }
};

export const DELETE = async (request: Request, { params }: { params: { id: string } }) => {
    try {
        const book = await prisma.book.delete({
            where: {
                id: Number(params.id)
            }
        });

        return NextResponse.json(book, { status: 200 });
    } catch (error) {
        console.error('Error deleting book:', error);
        return NextResponse.error('Failed to delete book', { status: 500 });
    }
};
