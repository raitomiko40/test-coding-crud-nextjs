// @ts-nocheck

import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export const POST = async (request: Request) => {
    try {
        const body = await request.json();
        console.log(body, 'ini body');

        let calculatedThickness = "";
        const totalPageInt = parseInt(body.total_page);

        if (totalPageInt <= 100) {
            calculatedThickness = "tipis";
        } else if (totalPageInt <= 200) {
            calculatedThickness = "sedang";
        } else {
            calculatedThickness = "tebal";
        }

        const createdBook = await prisma.book.create({
            data: {
                title: body.title,
                description: body.description,
                image_url: body.image_url,
                release_year: body.release_year,
                price: body.price,
                total_page: body.total_page,
                thickness: calculatedThickness,
                category_id: body.category_id,
            },
        });

        return NextResponse.json(createdBook, { status: 200 });
    } catch (error) {
        console.error("Error adding book:", error);
        return NextResponse.error();
    }
};

export const GET = async () => {
    try {
        const books = await prisma.book.findMany();

        return NextResponse.json(books, { status: 200 });
    } catch (error) {
        console.error('Error fetching books:', error);

        if (error instanceof Error && error.code === 'P2002') { 
            return NextResponse.error('Book already exists', { status: 409 });
        }
        return NextResponse.error('Failed to fetch books', { status: 500 });
    }
};
