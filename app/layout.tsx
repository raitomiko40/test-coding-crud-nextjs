'use client'

import { Inter } from 'next/font/google';
import './globals.css';
import NavBar from './utilities/navbar';
import React, { useState, useEffect } from 'react';

const inter = Inter({ subsets: ['latin'] });

interface RootLayoutProps {
  children: React.ReactNode;
}

export default function RootLayout({ children }: RootLayoutProps) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  useEffect(() => {

    const checkAuthentication = () => {
      const authToken = localStorage.getItem('authToken');
      if (authToken) {
        setIsLoggedIn(true);
      }
    };

    checkAuthentication();
  }, []);

  const handleLogout = () => {
    setIsLoggedIn(false);
  };

  return (
    <html lang="en">
      <body className={inter.className}>
        <NavBar isLoggedIn={isLoggedIn} onLogout={handleLogout} />
        {children}
      </body>
    </html>
  );
}
