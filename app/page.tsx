// pages/index.js
"use client"
import { useEffect, useState } from 'react';
import axios from 'axios';
import CategoryCardList from './components/categories/CategoryCardList';
import BookCardList from './components/books/BookCardList';;
import Banner from './components/banner/Banner';

const Home = () => {
  const [categories, setCategories] = useState([]);
  const [books, setBooks] = useState([]);
  const [error, setError] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axios.get('/api/categories');
        setCategories(response.data);
      } catch (error) {
        console.error('Error fetching categories:', error);
        setError('Failed to fetch categories');
      }
    };

    const checkAuthentication = () => {
      const authToken = localStorage.getItem('authToken');
      if (authToken) {
        setIsLoggedIn(true);
      }
    };

    fetchCategories();
    checkAuthentication();
  }, []);

  const handleLogout = () => {
    setIsLoggedIn(false);
  };

  useEffect(() => {
    const fetchBooks = async () => {
      try {
        const response = await axios.get('/api/books');
        setBooks(response.data);
      } catch (error) {
        console.error('Error fetching books:', error);
        setError('Failed to fetch books');
      }
    };

    fetchBooks();
  }, []);

  return (
    <div className="flex h-screen overflow-hidden">
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        <main>
          <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
            <Banner />
            <div className="grid grid-cols-12 gap-6">
              {error ? (
                <p className="text-red-500">{error}</p>
              ) : (
                <CategoryCardList categories={categories} />
              )}
              {error ? (
                <p className="text-red-500">{error}</p>
              ) : (
                <BookCardList books={books} />
              )}
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default Home;
