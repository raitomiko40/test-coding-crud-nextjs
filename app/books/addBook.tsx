"use client"
import { useState, useEffect } from "react"
import axios from "axios"
import { useRouter } from "next/navigation"

const addCategories = () => {
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [image_url, setImage_Url] = useState("")
    const [release_year, setRelease_year] = useState("")
    const [price, setPrice] = useState("")
    const [total_page, setTotalPage] = useState("")
  
    const [category_id, setCategory_id] = useState("")
    const [categories, setCategories] = useState([]);
    const [isOpen, setIsOpen] = useState(false)

    const router = useRouter()

    useEffect(() => {
        const fetchCategories = async () => {
            try {
                const response = await axios.get('/api/categories');
                setCategories(response.data);
            } catch (error) {
                console.error('Error fetching categories:', error);
            }
        };
  
        fetchCategories();
    }, []);

    const handleSubmit = async (e: { preventDefault: () => void }) => {
        e.preventDefault();
        try {
            const releaseYearInt = parseInt(release_year);
            if (isNaN(releaseYearInt) || releaseYearInt < 1980 || releaseYearInt > 2021) {
              console.error("Invalid release year. Please enter a year between 1980 and 2021.");
              alert("release year valid. Mohon masukkan tahun antara 1980 dan 2021.")
              return; // Do not proceed with form submission
            }
            // Ganti "nama" menjadi "name" dalam payload
            await axios.post('/api/books', {
                title: title,
                description: description,
                image_url: image_url,
                release_year: releaseYearInt,
                price: price,
                total_page: parseInt(total_page),
            
                category_id: parseInt(category_id),

            });
            setTitle("");
            setDescription("")
            setImage_Url("")
            setRelease_year("")
            setPrice("")
            setTotalPage("")
          
            setCategory_id("")
            router.refresh();
            setIsOpen(false);
        } catch (error) {
            console.error("Error adding category:", error);
            // Handle error, misalnya, tampilkan pesan kesalahan kepada pengguna
        }
    };

    const handleModal = () => {
        setIsOpen(!isOpen)
    }

  return (
    <div>
    <button className="btn" onClick={handleModal}>
        Add New
    </button>
    <div className={isOpen ? "modal modal-open" : "modal"}>
        <div className="modal-box">
            <h3 className="font-bold text-lg">Add New Book</h3>
            <form onSubmit={handleSubmit}>
                <div className="form-control w-full">
                    <label className="label font-bold">Book Title</label> 
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Book Title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-control w-full">
                    <label className="label font-bold">Description</label>
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Add Description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </div>
                <div className="form-control w-full">
                    <label className="label font-bold">Image</label>
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Add Image"
                        value={image_url}
                        onChange={(e) => setImage_Url(e.target.value)}
                    />
                </div>
                <div className="form-control w-full">
                    <label className="label font-bold">Release Year</label>
                    <input
                        type="number"
                        className="input input-bordered"
                        placeholder="Add Release Year"
                        value={release_year}
                        onChange={(e) => setRelease_year(e.target.value)}
                    />
                </div>
                <div className="form-control w-full">
                    <label className="label font-bold">Price</label>
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Add Price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                    />
                </div>
                <div className="form-control w-full">
                    <label className="label font-bold">Total Page</label>
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Add Total Page"
                        value={total_page}
                        onChange={(e) => setTotalPage(e.target.value)}
                    />
                </div>
             
               
                <div className="form-control w-full">
                            <label className="label font-bold">Category</label>
                            <select
                                className="input input-bordered"
                                value={category_id}
                                onChange={(e) => setCategory_id(e.target.value)}
                            >
                                <option value="" disabled>Select a category</option>
                                {categories.map((category) => (
                                    <option key={category.id} value={category.id}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>
                </div>
                <div className="modal-action">
                    <button type="button" className="btn" onClick={handleModal}>
                        Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
  )
}

export default addCategories