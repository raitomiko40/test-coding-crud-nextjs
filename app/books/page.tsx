// @ts-nocheck
"use client"

import { useEffect, useState } from 'react';
import axios from 'axios';
import AddBook from './addBook';
import DeleteBook from './deleteBook';
import UpdateBook from './updateBook';
import withAuth from '../hoc/withAuth';

const Books = ({ isLoggedIn, onLogout }) => {
  const [books, setBooks] = useState([]);

  const fetchBooks = async () => {
    try {
      const response = await axios.get('/api/books');
      setBooks(response.data);
    } catch (error) {
      console.error('Error fetching books:', error);
      setBooks([]);
    }
  };

  useEffect(() => {
    // Fetch data initially
    fetchBooks();

    // Polling every 5 seconds (adjust as needed)
    const intervalId = setInterval(fetchBooks, 5000);

    // Clean up the interval on component unmount
    return () => clearInterval(intervalId);
  }, []); // Dependency array is empty, so it runs only once on mount

  return (
    <div>
      <div className="mb-2">
        <AddBook />
      </div>

      <table className="table w-full">
        <thead>
          <tr>
            <th>id</th>
            <th>title</th>
            <th>description</th>
            <th>image_url</th>
            <th>release_year</th>
            <th>price</th>
            <th>total_page</th>
            <th>thickness</th>
            <th>category id</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {books.map((book, index) => (
            <tr key={book.id}>
              <td>{index + 1}</td>
              <td>{book.title}</td>
              <td>{book.description}</td>
              <td>{book.image_url}</td>
              <td>{book.release_year}</td>
              <td>{book.price}</td>
              <td>{book.total_page}</td>
              <td>{book.thickness}</td>
              <td>{book.category_id}</td>
              <td className="flex justify-center space-x-1">
                <UpdateBook book={book} />
                <DeleteBook book={book} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default withAuth(Books);
