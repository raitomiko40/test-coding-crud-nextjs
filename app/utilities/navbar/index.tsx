// @ts-nocheck

import Link from 'next/link';
import { useRouter } from 'next/navigation'

const NavBar = ({ isLoggedIn, onLogout }) => {
    const router = useRouter();
    const handleLogout = () => {
   
    localStorage.removeItem('authToken');
    onLogout();
    router.push('/');
  };

    return (
        <header className="sticky top-0 bg-white dark:bg-[#182235] border-b border-slate-200 dark:border-slate-700 z-30">
        <div className="px-4 sm:px-6 lg:px-8">
          <div className="flex items-center justify-between h-16 -mb-px">
            <div>
              <ul className='flex gap-5 '>
                {isLoggedIn ? (
                  <>
                    <li className='btn p-2'>
                      <Link href="/categories">Categories Crud</Link>
                    </li>
                    <li className='btn p-2'>
                      <Link href="/books">Books Crud</Link>
                    </li>
                  </>
                ) : (
                  <span></span>
                )}
              </ul>
            </div>
            <div className="flex items-center space-x-3">
              {isLoggedIn ? (
                <div>
                  <button className='btn btn-primary' onClick={handleLogout}>Logout</button>
                </div>
              ) : (       
                <>
                  <div>
                    <Link 
                      className='btn btn-primary'
                      href='/auth/login'
                    >
                      Login
                    </Link>
                  </div>
                  <div>
                    <Link 
                      href='/auth/register'
                      className='btn btn-primary'
                      >                   
                      Register
                    </Link>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
        </header>
    )
}

export default NavBar