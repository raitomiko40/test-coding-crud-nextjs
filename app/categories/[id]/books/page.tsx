"use client"

import {FC} from "react"
interface pageProps{
    params : {id: string}
}

import React, { useState, useEffect } from 'react';

const CategoryDetail: FC<pageProps> = ({params}) => {
  const [category, setCategory] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const id = params.id

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:3000/api/categories/${id}`);
        const data = await response.json();
        setCategory(data);
      } catch (error) {
        setError(error.message || 'Internal Server Error');
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <div className='flex flex-col col-span-full sm:col-span-6 bg-white shadow-lg'>
        <div className='px-5 py-4 border-b border-slate-100 dark:border-slate-700'>
            <div>
            <h2 className="font-semibold text-slate-800 dark:text-slate-100">
                List Books on Category {category.name}
            </h2>
            <ul>
                {category.books.map((book) => (
                <li key={book.id}>{book.title}</li>
                ))}
            </ul>
            </div>
            <button className="btn btn-primary">back to home</button>
        </div>
    </div>
  );
};

export default CategoryDetail;
