// @ts-nocheck
import { useState } from "react"
import axios from "axios"
import { useRouter } from "next/navigation"

const AddCategories = () => {
    const [name, setName] = useState("")
    const [isOpen, setIsOpen] = useState(false)
    const router = useRouter()

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await axios.post('/api/categories', {
                name: name,
            });
            setName("");
            router.refresh();
            setIsOpen(false);
            alert("Category added successfully!"); // Tampilkan alert sukses
        } catch (error) {
            console.error("Error adding category:", error);
            alert("Failed to add category. Please try again."); // Tampilkan alert error
        }
    };

    const handleModal = () => {
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <button className="btn" onClick={handleModal}>
                Add New
            </button>
            <div className={isOpen ? "modal modal-open" : "modal"}>
                <div className="modal-box">
                    <h3 className="font-bold text-lg">Add New Category</h3>
                    <form onSubmit={handleSubmit}>
                        <div className="form-control w-full">
                            <label className="label font-bold">Category Name</label>
                            <input
                                type="text"
                                className="input input-bordered"
                                placeholder="Category Name"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </div>
                        <div className="modal-action">
                            <button type="button" className="btn" onClick={handleModal}>
                                Close
                            </button>
                            <button type="submit" className="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AddCategories
