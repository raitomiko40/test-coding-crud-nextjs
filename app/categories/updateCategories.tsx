"use client"
import { useState } from "react"
import axios from "axios"
import { useRouter } from "next/navigation"

type Category = {
    id: number,
    name: string
}

const updateCategories = ({category} : { category : Category} ) => {
    const [name, setName] = useState(category.name)
    const [isOpen, setIsOpen] = useState(false)

    const router = useRouter()


    const handleUpdate = async (e: { preventDefault: () => void }) => {
        e.preventDefault();
        try {
            // Ganti "nama" menjadi "name" dalam payload
            await axios.patch(`/api/categories/${category.id}`, {
                name: name,
            });
            setName("");
            router.refresh();
            setIsOpen(false);
        } catch (error) {
            console.error("Error adding category:", error);
        }
    };

    const handleModal = () => {
        setIsOpen(!isOpen)
    }

  return (
    <div>
    <button className="btn btn-info btn-sm  " onClick={handleModal}>
        Edit
    </button>
    <div className={isOpen ? "modal modal-open" : "modal"}>
        <div className="modal-box">
            <h3 className="font-bold text-lg">Add New Category</h3>
            <form onSubmit={handleUpdate}>
                <div className="form-control w-full">
                    <label className="label font-bold">Category Name</label>
                    {/* Pastikan input terhubung dengan state 'name' */}
                    <input
                        type="text"
                        className="input input-bordered"
                        placeholder="Category Name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </div>
                <div className="modal-action">
                    <button type="button" className="btn" onClick={handleModal}>
                        Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
  )
}

export default updateCategories