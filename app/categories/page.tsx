// @ts-nocheck
"use client"

import { useState, useEffect } from 'react';
import axios from 'axios';
import AddCategories from "./addCategories";
import DeleteCategory from "./deleteCategories";
import UpdateCategories from "./updateCategories";
import withAuth from '../hoc/withAuth';

const formatDate = (dateString) => {
  const options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
  return new Date(dateString).toLocaleDateString('id-ID', options);
};

const Categories = ({ isLoggedIn, onLogout }) => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axios.get('/api/categories');
        setCategories(response.data);
      } catch (error) {
        console.error('Error fetching category:', error);
        setCategories([]);
      }
    };  
    fetchCategories();
    const intervalId = setInterval(fetchCategories, 5000);

 
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div>
      <div className="mb-2">
        <AddCategories />
      </div>

      <table className="table w-full">
        <thead>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category, index) => (
            <tr key={category.id}>
              <td>{index + 1}</td>
              <td>{category.name}</td>
              <td>{formatDate(category.created_at)}</td>
              <td>{formatDate(category.updated_at)}</td>
              <td className="flex justify-center space-x-1">
                <UpdateCategories category={category} />
                <DeleteCategory category={category} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default withAuth(Categories);
