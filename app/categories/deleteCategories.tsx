
import { useState } from "react"
import axios from "axios"
import { useRouter } from "next/navigation"

type Category = {
    id: number,
    name: string
}

const deleteCategories = ( {category} : { category : Category}) => {
    const [isOpen, setIsOpen] = useState(false)

    const router = useRouter()


    const handleDelete = async (categoryId: number) => {
 
            await axios.delete(`/api/categories/${categoryId}`, );
            router.refresh();
            setIsOpen(false);
    
    };

    const handleModal = () => {
        setIsOpen(!isOpen)
    }

  return (
    <div>
    <button className="btn btn-error btn-sm" onClick={handleModal}>
        Delete
    </button>
    <div className={isOpen ? "modal modal-open" : "modal"}>
        <div className="modal-box">
            <h3 className="font-bold text-lg">Are you sure to delete {category.name}</h3>               
                <div className="modal-action">
                    <button type="button" className="btn" onClick={handleModal}>
                        No
                    </button>
                    <button type="button" className="btn btn-primary" onClick={()=>handleDelete(category.id)}>
                        Yes
                    </button>
                </div>
        </div>
    </div>
</div>
  )
}

export default deleteCategories