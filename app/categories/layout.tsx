export const metadata = {
    title : "Categories"
}

const CategoriesLayout = ({children} : { children: React.ReactNode}) => {
  return (
    <div className="py-10 px-10">{children}</div>
  )
}

export default CategoriesLayout