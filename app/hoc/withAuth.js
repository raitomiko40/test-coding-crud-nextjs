// pages/withAuth.js
import { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';

const withAuth = (WrappedComponent) => {
  const Auth = (props) => {
    const router = useRouter();
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
      const authToken = localStorage.getItem('authToken');
      setIsLoggedIn(!!authToken);
      
      if (!authToken) {
        console.log('Pengguna belum login. Tindakan tambahan dapat ditambahkan di sini.');
        router.push('/');
      }
    }, [router.pathname]); 

    return <WrappedComponent {...props} isLoggedIn={isLoggedIn} onLogout={() => setIsLoggedIn(false)} />;
  };

  return Auth;
};

export default withAuth;
