// @ts-nocheck
import React, { useState } from 'react';
import Link from 'next/link';

const BookCardList = ({ books }) => {
  const [selectedYear, setSelectedYear] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  const handleYearChange = (event) => {
    const year = event.target.value === 'all' ? null : parseInt(event.target.value);
    setSelectedYear(year);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredBooks = books
    .filter((book) => !selectedYear || book.release_year === selectedYear)
    .filter((book) => book.title.toLowerCase().includes(searchTerm.toLowerCase()));

  return (
    <div className='flex flex-col col-span-full sm:col-span-6 bg-white shadow-lg'>
      <div className='px-5 py-4 border-b border-slate-100 dark:border-slate-700'>
        <h2 className="font-semibold text-slate-800 dark:text-slate-100">Books List</h2>
      <div className='px-5 py-3 flex flex-wrap space-x-4'>
        {/* Render a dropdown for selecting years from 1980 to 2021 */}
        <label className='flex items-center space-x-2'>
          Select Year:
          <select
            value={selectedYear || 'all'}
            onChange={handleYearChange}
          >
            <option value='all'>All Years</option>
            {Array.from({ length: 42 }, (_, index) => 1980 + index).map((year) => (
              <option key={year} value={year}>
                {year}
              </option>
            ))}
          </select>
        </label>
        
        {/* Add a text input for searching */}
        <label className='flex items-center space-x-2'>
          Search:
          <input
            className='border border-black'
            type='text'
            value={searchTerm}
            onChange={handleSearchChange}
          />
        </label>
      </div>
      </div>

      {/* Render the filtered list of books */}
      <div className='px-5 py-3'>
        {filteredBooks.map((book) => (
          <div key={book.id}>
            <Link
              className='flex flex-wrap text-blue-500 hover:underline'
              href={`/book/${book.id}/books`}
            >
              {book.title}
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BookCardList;
