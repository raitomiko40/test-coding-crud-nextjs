// components/BookList.jsx
"use client";
    type Category = {
        id: number,
        name: string,
        books: Array<Book> 
    }

    type Book = {
        id: number,
        title: string,
        description: string,
    }

    const BookList = ({ category }: { category: Category }) => {
        if (!category || !category.books) {
            return null; 
        }
    return (
        <div className='flex flex-col col-span-full sm:col-span-6 bg-white  shadow-lg'>
            <div className='px-5 py-4 border-b border-slate-100 dark:border-slate-700'>
                <h2 className="font-semibold text-slate-800 dark:text-slate-100">{category.name} Books</h2>
            </div>
            <div className='px-5 py-3'>
                {category.books.map((book) => (
                    <div key={book.id} className="mb-2">
                        <span className="flex flex-wrap">{book.title}</span>
                  
                    </div>
                ))}
            </div>
        </div>
    );
};

export default BookList;
