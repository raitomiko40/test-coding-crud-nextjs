// @ts-nocheck

import React, { useState } from 'react';
import Link from 'next/link';

const CategoryCardList = ({ categories }) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredCategories = categories
    .filter((category) => category.name.toLowerCase().includes(searchTerm.toLowerCase()));

  return (
    <div className='flex flex-col col-span-full sm:col-span-6 bg-white shadow-lg'>
      <div className='px-5 py-4 border-b border-slate-100 dark:border-slate-700'>
        <h2 className="font-semibold text-slate-800 dark:text-slate-100">Categories List</h2>
        <label className='flex items-center space-x-2'>
          Search:
          <input
            type='text'
            value={searchTerm}
            onChange={handleSearchChange}
            className='border border-black px-2 py-1'
          />
        </label>
      </div>
      <div className="px-5 py-3">
        {filteredCategories.map((category) => (
          <div key={category.id}>
            <Link 
              className="flex flex-wrap text-blue-500 hover:underline"
              href={`/categories/${category.id}/books`}
            >
              {category.name}
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CategoryCardList;
