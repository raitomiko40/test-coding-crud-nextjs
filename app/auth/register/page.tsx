// pages/index.js
"use client"

import React, { useState } from 'react';
import Link from 'next/link';

const RegisterPage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [alert, setAlert] = useState(null);

  const handleRegister = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('/api/auth/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Registration successful:', data);
        setAlert({ type: 'success', message: 'Registration successful!' });

      } else {
        const errorData = await response.json();
        console.error('Registration failed:', errorData);
        setAlert({ type: 'error', message: 'Registration failed. Please try again.' });
      }
    } catch (error) {
      console.error('Error during registration:', error);
      setAlert({ type: 'error', message: 'Internal Server Error. Please try again later.' });
      // Handle unexpected error during registration
    }
  };

  return (
    <div className="relative flex flex-col justify-center h-screen overflow-hidden">
      <div className="w-full p-6 m-auto bg-white rounded-md shadow-md ring-2 ring-gray-800/50 lg:max-w-lg">
        <h1 className="text-3xl font-semibold text-center text-gray-700">Register</h1>
        {alert && (
          <div className={`alert ${alert.type === 'success' ? 'alert-success' : 'alert-error'}`}>
            {alert.message}
          </div>
        )}
        <form className="space-y-4" onSubmit={handleRegister}>
          <div>
            <label className="label">
              <span className="text-base label-text">Username</span>
            </label>
            <input
              type="text"
              placeholder="Username"
              className="w-full input input-bordered"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <label className="label">
              <span className="text-base label-text">Password</span>
            </label>
            <input
              type="password"
              placeholder="Enter Password"
              className="w-full input input-bordered"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          
          <div>
            <button type="submit" className="btn btn-block">
              Register
            </button>
          </div>
          <div className='flex gap-12'>
            <Link href="/auth/login" className=" text-xs text-gray-600 hover:underline hover:text-blue-600">
              Already have an account? Login now...
            </Link>
            <Link href="/" className=" text-lg text-gray-600 hover:underline hover:text-blue-600">
              Back to Home
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default RegisterPage;
