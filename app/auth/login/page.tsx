// app/auth/login/page.tsx

 "use client"
import React, { useState } from 'react';
import { useRouter } from 'next/navigation';
import Link from 'next/link';

const LoginPage = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const router = useRouter();
    const [isLoggedIn, setIsLoggedIn] = useState(false);
  
    const handleLogin = async (e) => {
      e.preventDefault();
  
      try {
        const response = await fetch('/api/auth/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password }),
        });
  
        if (response.ok) {
          const data = await response.json();
          console.log('Login successful:', data);
  
          // Simpan token di penyimpanan lokal
          localStorage.setItem('authToken', data.token);
  
          // Atur nilai isLoggedIn menjadi true
          setIsLoggedIn(true);
  
          // Redirect to home page after successful login
          router.push('/');
        } else {
          const errorData = await response.json();
          console.error('Login failed:', errorData);
          // Handle login failure (show error message, etc.)
        }
      } catch (error) {
        console.error('Error during login:', error);
        // Handle unexpected error during login
      }
    };

  return (
    <div className="relative flex flex-col justify-center h-screen overflow-hidden">
      <div className="w-full p-6 m-auto bg-white rounded-md shadow-md ring-2 ring-gray-800/50 lg:max-w-lg">
        <h1 className="text-3xl font-semibold text-center text-gray-700">Login</h1>
        <form className="space-y-4" onSubmit={handleLogin}>
          <div>
            <label className="label">
              <span className="text-base label-text">Username</span>
            </label>
            <input
              type="text"
              placeholder="Username"
              className="w-full input input-bordered"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <label className="label">
              <span className="text-base label-text">Password</span>
            </label>
            <input
              type="password"
              placeholder="Enter Password"
              className="w-full input input-bordered"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <div>
            <button type="submit" className="btn btn-block">
              Login
            </button>
          </div>
          <div className="flex gap-12">
            <Link href="/auth/register" className="text-xs text-gray-600 hover:underline hover:text-blue-600">
              Don't have an account? Register now...
            </Link>
            <Link href="/" className="text-lg text-gray-600 hover:underline hover:text-blue-600">
              Back to Home
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
